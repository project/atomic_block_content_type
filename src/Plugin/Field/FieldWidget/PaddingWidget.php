<?php

namespace Drupal\atomic_block_content_type\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'AddressDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "padding_widget",
 *   label = @Translation("Padding widget"),
 *   field_types = {
 *     "padding"
 *   }
 * )
 */
class PaddingWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   *
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $formState) {

    $element['#type'] = 'fieldset';
    $element['#attributes']['class'] = ['padding-fieldset'];
    $element['#attached']['library'][] = 'atomic_block_content_type/padding-widget';
    $element['left'] = [
      '#type' => 'number',
      '#title' => $this->t('Left'),
      '#min' => 0,
      '#max' => 1000,
      '#step' => 1,
      '#required' => FALSE,
      //      '#field_prefix' => $this->t('Left'),
      '#default_value' => $items[$delta]->left ?? 0,
      '#field_suffix' => $this->t('px'),
    ];

    $element['top'] = [
      '#type' => 'number',
      '#title' => $this->t('Top'),
      '#min' => 0,
      '#max' => 1000,
      '#step' => 1,
      '#required' => FALSE,
      '#default_value' => $items[$delta]->top ?? 0,
      '#field_suffix' => $this->t('px'),
    ];

    $element['right'] = [
      '#type' => 'number',
      '#title' => $this->t('Right'),
      '#min' => 0,
      '#max' => 1000,
      '#step' => 1,
      '#required' => FALSE,
      '#default_value' => $items[$delta]->right ?? 0,
      '#field_suffix' => $this->t('px'),
    ];

    $element['bottom'] = [
      '#type' => 'number',
      '#title' => $this->t('Bottom'),
      '#min' => 0,
      '#max' => 1000,
      '#step' => 1,
      '#required' => FALSE,
      '#default_value' => $items[$delta]->bottom ?? 0,
      '#field_suffix' => $this->t('px'),
    ];

    return $element;
  }

} // class
