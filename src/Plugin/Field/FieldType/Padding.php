<?php

namespace Drupal\atomic_block_content_type\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'Padding' field type.
 *
 * @FieldType(
 *   id = "padding",
 *   label = @Translation("Padding or Margin"),
 *   description = @Translation("Padding settings."),
 *   category = @Translation("atomic_block_content_type"),
 *   default_widget = "padding_widget",
 *   default_formatter = "padding_formatter"
 * )
 */
class Padding extends FieldItemBase {

  /**
   * Field type properties definition.
   *
   * Inside this method we defines all the fields (properties) that our
   * custom field type will have.
   *
   * Here there is a list of allowed property types: https://goo.gl/sIBBgO
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties['left'] = DataDefinition::create('integer')
      ->setLabel(t('Left'));
    $properties['top'] = DataDefinition::create('integer')
      ->setLabel(t('Top'));
    $properties['right'] = DataDefinition::create('integer')
      ->setLabel(t('Right'));
    $properties['bottom'] = DataDefinition::create('integer')
      ->setLabel(t('Bottom'));

    return $properties;
  }

  /**
   * Field type schema definition.
   *
   * Inside this method we defines the database schema used to store data for
   * our field type.
   *
   * Here there is a list of allowed column types: https://goo.gl/YY3G7s
   */
  public static function schema(StorageDefinition $storage) {

    $columns = [];
    $columns['left'] = [
      'type' => 'int',
      'unsigned' => FALSE,
      'size' => 'normal',
    ];
    $columns['top'] = [
      'type' => 'int',
      'unsigned' => FALSE,
      'size' => 'normal',
    ];
    $columns['right'] = [
      'type' => 'int',
      'unsigned' => FALSE,
      'size' => 'normal',
    ];
    $columns['bottom'] = [
      'type' => 'int',
      'unsigned' => FALSE,
      'size' => 'normal',
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

//  /**
//   * Define when the field type is empty.
//   *
//   * This method is important and used internally by Drupal. Take a moment
//   * to define when the field fype must be considered empty.
//   */
//  public function isEmpty() {
//
//    $isEmpty =
//      empty($this->get('left')->getValue()) &&
//      empty($this->get('top')->getValue()) &&
//      empty($this->get('right')->getValue()) &&
//      empty($this->get('bottom')->getValue());
//
//    return $isEmpty;
//  }

}
