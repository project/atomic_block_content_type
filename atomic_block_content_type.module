<?php

use Mexitek\PHPColors\Color;

/**
 * Implements hook_theme().
 */
function atomic_block_content_type_theme($existing, $type, $theme, $path) {
  return [
    'block__block_content__abct_list' => [
      'template' => 'block--block-content--abct-list',
      'base hook' => 'block',
    ],
    'block__block_content__abct_text' => [
      'template' => 'block--block-content--abct-text',
      'base hook' => 'block',
    ],
    'block__block_content__abct_title' => [
      'template' => 'block--block-content--abct-title',
      'base hook' => 'block',
    ],
    'block__block_content__abct_background_image' => [
      'template' => 'block--block-content--abct-background-image',
      'base hook' => 'block',
    ],
    'block__block_content__abct_button' => [
      'template' => 'block--block-content--abct-button',
      'base hook' => 'block',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function atomic_block_content_type_preprocess_block(&$variables) {
  if ($variables['elements']['#base_plugin_id'] == "inline_block" &&
    in_array($variables['elements']['#derivative_plugin_id'],['abct_list', 'abct_text', 'abct_title', 'abct_button'])) {
    /** @var \Drupal\Core\Field\FieldItemList $text_color_field_list_item */
    $text_color_field_list_item = $variables['elements']['content']['field_abct_text_color']['#items'];
    if (isset($text_color_field_list_item)) {
      $color = $text_color_field_list_item->getValue()[0]['color'];
      $opacity = $text_color_field_list_item->getValue()[0]['opacity'];
      $color_opacity = hexToRgba($color, $opacity);
      $variables['elements']['text_color_opacity'] = $color_opacity;
    }

    $bg_color_field_list_item = $variables['elements']['content']['field_abct_bg_color']['#items'];
    if (isset($bg_color_field_list_item)) {
      $bg_color = $bg_color_field_list_item->getValue()[0]['color'];
      $bg_opacity = $bg_color_field_list_item->getValue()[0]['opacity'];
      $ba_color_opacity = hexToRgba($bg_color, $bg_opacity);
      $variables['elements']['bg_color_opacity'] = $ba_color_opacity;
    }

    /** @var \Drupal\Core\Field\FieldItemList $vertical_align_field_list_item */
    $vertical_align_field_list_item = $variables['elements']['content']['field_abct_vertical_align']['#items'];
    if (isset($vertical_align_field_list_item)) {
      $variables['elements']['vertical_align_class'] = 'atomic-vertical-' . $vertical_align_field_list_item->getString();
    }

    /** @var \Drupal\Core\Field\FieldItemList $padding_field_list_item */
    $padding_field_list_item = $variables['elements']['content']['field_abct_padding']['#items'];

    if (isset($padding_field_list_item)) {
      $paddings = $padding_field_list_item->getValue()[0];
      $variables['elements']['padding'] = $paddings['top'] . 'px ' . $paddings['right'] . 'px ' . $paddings['bottom'] . 'px ' . $paddings['left'] . 'px ';
    }

    /** @var \Drupal\Core\Field\FieldItemList $padding_field_list_item */
    $content_padding_field_list_item = $variables['elements']['content']['field_abct_content_padding']['#items'];

    if (isset($content_padding_field_list_item)) {
      $content_paddings = $content_padding_field_list_item->getValue()[0];
      $variables['elements']['content_padding'] = $content_paddings['top'] . 'px ' . $content_paddings['right'] . 'px ' . $content_paddings['bottom'] . 'px ' . $content_paddings['left'] . 'px ';
    }

    /** @var \Drupal\Core\Field\FieldItemList $hover_display_field_list_item */
    $hover_display_field_list_item = $variables['elements']['content']['field_abct_hover_display']['#items'];

    if (isset($hover_display_field_list_item) && $hover_display_field_list_item->getString() == '1') {
      $variables['elements']['hover_class'] = 'atomic-hover-display';
    }
  }

  if ($variables['elements']['#base_plugin_id'] == "inline_block" && $variables['elements']['#derivative_plugin_id'] == 'abct_button') {

    $button_bg_color_field_list_item = $variables['elements']['content']['field_abct_button_bg_color']['#items'];
    if (isset($button_bg_color_field_list_item)) {
      $button_bg_color = $button_bg_color_field_list_item->getValue()[0]['color'];
      $button_bg_opacity = $button_bg_color_field_list_item->getValue()[0]['opacity'];
      $button_bg_color_opacity = hexToRgba($button_bg_color, $button_bg_opacity);
      $variables['elements']['button_bg_color_opacity'] = $button_bg_color_opacity;
    }
  }
}

function hexToRgba($hex, $opacity) {
  /** @var \Mexitek\PHPColors\Color $color */
  $color = new Color($hex);
  $color_rgb_array = $color->getRgb();
  return 'rgba(' . $color_rgb_array['R'] . ',' . $color_rgb_array['G'] . ',' . $color_rgb_array['B'] . ',' . $opacity . ')';
}

